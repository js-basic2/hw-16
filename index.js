"use strict";
let n = prompt("Введите число", 3);
function fib(n) {
  if (n >= 0) {
  let F0 = 1;
  let F1 = 1;
  for (let i = 3; i <= n; i++) {
    let F2 = F0 + F1;
    F0 = F1;
    F1 = F2;
  }
  return F1;
}
if (n <= 0) {
  let F0 = 1;
  let F1 = -1;
  for (let i = -3; i >= n; i--) {
    let F2 = F0 - F1;
    F0 = F1;
    F1 = F2;
  }
  return F1;
}
}

console.log(fib(n));
